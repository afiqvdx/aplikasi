<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Author;
use App\Mail\WelcomeEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BookController extends Controller
{
    public function index(){
        $books = Book::select(['id','title','price'])->
        with('authors')
        ->paginate(10);

        $email_data = [
            'code' => 'ABCXYZ12345',
            'name' => 'Encik Afiq',
            'title'=> 'harilahir'
        ];

        Mail::to('afiqvdx20@gmail.com')->send(new WelcomeEmail($email_data));

        return view('books.listing',[
            'books' => $books
        ]);
    }

    public function authors(){
        $authors = Author::with('books')->get();

        return view('books.authors', [
            'authors' => $authors
        ]);
    }

    public function author( $id ){
        $author = Author::with('books')->find($id);

        return view('books.author-single', [
            'author' => $author
        ]);  
    }

    public function edit($id){
        $book = Book::findOrFail($id);

        return view('books.edit',[
            'book' => $book
        ]);
    }

    public function update(Request $request, $id){

        $book = Book::findOrFail($id);

        $validated_data = $request->validate([
            'title' => 'required|min:5|max:255',
            'price' => 'required|numeric',
            'sypnosis'=> 'required|min:20|max:1000'
        ]);

        $book->title = $request->title;
        $book->price = $request->price;
        $book->sypnosis = $request->sypnosis;

        $book->save();

        return back()->with('success', 'Book has been updated');
    }

    public function create(){
        return view('books.create');
    }

    public function store(Request $request){
        // dd($request);

        $validated_data = $request->validate([
            'title' => 'required|min:5|max:255',
            'price' => 'required|numeric',
            'sypnosis'=> 'required|min:20|max:1000'
        ]);

        $book = Book::create($validated_data);
        
        $email_data = [
            'code' => 'ABCXYZ12345',
            'name' => 'Encik Afiq',
            'title'=> 'harilahir'
        ];
        

         Mail::to('afiqvdx20@gmail.com')->send(new WelcomeEmail($email_data));
        
        return redirect()->route('book-listing')->with('success','Your Book has been added');
    
    }

    public function show($id){

        $book = Book::with('authors')->find($id);

        return view('books.single', ['book' => $book]);
        //dd($book);

        // echo "<strong>Title: </strong>". $book->title ."<br>";
        // echo "<strong>Price:</strong> RM". $book->price ."<br>";
        // echo "<strong>Sypnosis: </strong>". nl2br($book->title) ."<br>";
        
    }

    public function destroy($id){
        $book = Book::findOrFail($id);

        $book->delete();
        return redirect()->route('book-listing')->with('success','Your Book has been deleted');

    }
}
