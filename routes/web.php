<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\JadualController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

/**
 * BOOKS CONTROLLER
 * 
 * 
 */
Route::get('/book/create', [BookController::class, 'create'])->name('book-create');


Route::get('/book/{id}', [BookController::class, 'show'])->name('book-single');

Route::get('/book/{id}/edit', [BookController::class, 'edit'])->name('book-edit');

Route::post('/book/{id}', [BookController::class, 'update'])->name('book-update');

Route::delete('/book/{id}', [BookController::class, 'destroy'])->name('book-destroy');

Route::get('/books', [BookController::class, 'index'])->name('book-listing');

Route::post('/book', [BookController::class, 'store'])->name('book-store');

Route::get('/author/{id}', [BookController::class, 'author'])->name('author-single');


//redirect
Route:: redirect('/afiq', '/');

//view
Route::view('/pelajar', 'pelajar', 
[
    'nama' =>'Afiq'
]);

Route::get('/kelas', function () {
    return view('go');
});

Route::post('/kelas', function () {
    echo "POst-- KELAS";
});

Route::delete('/kelas',function(){

});

Route::put('/kelas',function(){

});

Route::patch('/kelas',function(){

});

Route::options('/kelas',function(){

});

Route::get('/welcome/{nama?}', 
    
    function($nama = 'Saudara / Saudari'){
    echo "<h1>Selamat Datang $nama</h1>";
});

//controller

Route::get('/jadual/{subjek}',
[JadualController::class, 
'index']);

Route::resource('user', UserController::class);

Route::get('/authors', [BookController::class, 'authors'])->name('author-listing');

Route::get('/test', [DashboardController::class, 'test']);

Route::get('/email/welcome', function(){
    return new App\Mail\WelcomeEmail();
});

