<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome To Aplikasi</title>
</head>

<style>
    body{background-color: #eee}
    table{
        background-color: #fff;
        width:100%;
        max-width: 800px;
        margin:auto;
        padding:30px;
    }
    h1{
        margin-top:0px;
    }

    a.btn{
        background-color: lightblue;
        text-decoration: none;
        padding: 10px 20px;
        font-size: 10px;
        color: #fff;
        border-radius: 10px;
        margin-top:20px;
        display:inline-block;
    }
</style>
<body>
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td>
               <!-- email content here-->
               <h1>Selamat Datang {{$name}}</h1>

               <p>{{ $title }} telah di tambah</p>
        
            
               <a href="" class="btn">KLIK DI SINI</a>
            </td>
        </tr>
    </table>
</body>
</html>