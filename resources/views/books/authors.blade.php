@extends ('layout.main')

@section('maizgulai')

<div class="row">
    <div class="col">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th colspan="2">Name</th>
                </tr>
            </thead>
            <tbody>

            @foreach( $authors as $author )
                <tr>
                    <td colspan="2"><strong>Books By  {{$author->name}}</strong></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td>
                        @if($author->books->count() < 1 )
                            <em>author Does not have any books listed here</em>
                        @else
                        <ol>
                        @foreach ($author->books as $book)
                            <li><a href="{{ route('book-single', $book->id) }}">{{$book->title}}</a></li>
                        @endforeach
                        </ol>
                    @endif

                    
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection