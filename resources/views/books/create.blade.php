@extends ('layout.main')

@section('maizgulai')

<div class="row mt-5 mb-5">

@if($errors->any())
<p class="alert alert-danger">Please check your input</p>
@endif

<form action="{{ route('book-store') }}" method="POST">
@csrf



<div class="row mt-5">
    <div class="div col">
        <a href="{{ route('book-listing') }}">Back to Book Listing</a>
        <h4>Add New Book</h4>

  <div class="mb-3">
    <label for="title" class="form-label">Title</label>
    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}" >
    @error('title')
      <div class="invalid-feedback">{{$message}}</div>
     @enderror
  </div>


  <div class="mb-3">
    <label for="price" class="form-label">Price</label>
    <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" value="{{ old('price') }}" >
    @error('price')
      <div class="invalid-feedback">{{$message}}</div>
  @enderror
  </div>



  <div class="mb-3">
    <label for="sypnosis" class="form-label">Synopsis</label>
    <textarea class="form-control @error('sypnosis') is-invalid @enderror" id="sypnosis" name="sypnosis" rows="10">{{ old('sypnosis') }}</textarea>
    @error('sypnosis')
      <div class="invalid-feedback">{{$message}}</div>
    @enderror
  </div>



  <button type="submit" class="btn btn-primary">SAVE</button>
</form>

</div>
    </div>
</div>

@endsection