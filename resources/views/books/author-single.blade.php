@extends ('layout.main')

@section('maizgulai')

<div class="row">
    <div class="col">
        <h4>{{ $author->name }}</h4>
        <table class="table table-striped">
        
            <tbody>

            
                <tr>
                    <td colspan="2"><strong>Books By  {{$author->name}}</strong></td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td>
                        @if($author->books->count() < 1 )
                            <em>author Does not have any books listed here</em>
                        @else
                        <ol>
                        @foreach ($author->books as $book)
                            <li><a href="{{ route('book-single', $book->id) }}">{{$book->title}}</a></li>
                        @endforeach
                        </ol>
                    @endif

                    
                    </td>
                </tr>
           
            </tbody>
        </table>
    </div>
</div>

@endsection